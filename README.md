# SacredTexts.Faith Frontend

## Deploy to Swarm

1. Build Image on all Swarm Nodes

	This image is not hosted on a registry, so it must be built onto every swarm node.
	
	`docker-compose build --no-cache`

1. Make Sure Network is Available
	
	This application uses the external "proxy" network
	
	Check if it's available: `docker network ls`
	
	If not, create it: `docker network create proxy -d overlay`
	
1. Remove Existing Stack
	
	If the stack is already deployed on the swarm, it should be removed before redeploy:
	
	`docker stack rm stfront`
1. Deploy to Swarm

	`docker stack deploy -c docker-compose.yml stfront`
