import React from 'react'
import { connect } from 'react-redux'
import { removeOrigin } from './router'
import { fetchTerm } from '../lib/actions'

class QuoteTerm extends React.Component {
	componentWillMount(){
		let promise = this.props.dispatch( fetchTerm( this.props.params ) )
		promise.then(
			() => {
				this.props.history.push( removeOrigin( this.props.quotes[0].termlink ) )
			}
		)
	}
	render(){
		return null
	}
}

export default connect(
	( state = {}, ownProps ) => {
		return { quotes: state.quotes, history: ownProps.history, params: ownProps.match.params }
	}
)( QuoteTerm )