import React from 'react'
import ArrowNav from './arrow-nav.js'
import CSSTransitionGroup from './css-transition-group'
import FirstChild from './first-child'
import Info from './info'
import { fetchQuote, fetchTerm } from '../lib/actions.js'
import { connect } from 'react-redux'
import './picture-quote.sass'

class PictureQuote extends React.Component {
	get dates(){
		let attribution = this.props.quote.attribution
		if( attribution.contemporary_voice )
			return 'Contemporary'
		else if( attribution.dates )
			return attribution.dates
		else
			return false
	}
	get index(){
		let quote = this.props.quote
		let quotes = this.props.quotes
		if( quotes ){
			let quoteIndex = quotes.findIndex( q => {
				return q.id == quote.id
			} )
			return quoteIndex
		}
		return 0
	}
	get nextURL(){
		return this.adjacentURL( 'next' )
	}
	get prevURL(){
		return this.adjacentURL( 'prev' )
	}
	adjacentURL( direction ){
		if( this.props.quotes ){
			let difference = 'next' == direction ? +1 : -1
			let index = this.index + difference
			let nextQuote = this.props.quotes[index]
			return nextQuote ? nextQuote.termlink : false
		} else {
			return this.props[direction]
		}
	}
	componentDidMount(){
		this.props.fetchQuote( this.props.match.params )
		let params = this.props.match.params
		if( params.tax && params.term ){
			this.props.fetchTerm( params )
		}
	}
	componentDidUpdate( prevProps ){
		if( prevProps.location.pathname != this.props.location.pathname ){
			this.props.fetchQuote( this.props.match.params )
		}
	}
	render(){
		if( ! this.props.quote ) return null;
		const quote = this.props.quote
		let attribution = quote.attribution
		if( typeof attribution != 'string' ){
			attribution = [
				<span key='person'>{quote.attribution.person}</span>,
				this.dates && <span key='dates'> ({this.dates}) </span>,
				<br key='break' />,
				<span key="publication">{quote.attribution.publication}</span>,
				quote.religion && quote.religion[0] && <div key="wisdom_tradition">{quote.religion[0]}</div>
			]
		}
		return (
			<main className="main">
				<figure className="picture-quote">
					<blockquote key="quote" dangerouslySetInnerHTML={quote.text_quote} />
					<figcaption>
						<cite>{attribution}</cite>
					</figcaption>
				</figure>
				<CSSTransitionGroup component={FirstChild}>
					{ this.props.infoOpen && <Info {...this.props} /> }
				</CSSTransitionGroup>
				<ArrowNav quote={ this.props.quote } prev={this.prevURL} next={this.nextURL} />
			</main>
		)
	}
}

export default connect(
	( state = {} ) => {
		let quote = state.quote
		return {
			quote: quote,
			infoOpen: state.infoOpen,
			quotes: state.quotes,
			background: state.background,
			next: state.next,
			prev: state.prev,
		}
	},
	dispatch => {
		return {
			fetchQuote: params => { dispatch( fetchQuote( params ) ) },
			fetchTerm:  params => { dispatch( fetchTerm( params ) ) }
		}
	}
)( PictureQuote )