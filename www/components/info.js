import React from 'react'
import './info.sass'
import Image from '../lib/image'

export default class extends React.Component {
	renderResource( r ){
		if( r.url ){
			return (
				<a target="_blank" href={r.url}>{r.text}</a>
			)
		} else {
			return r.text
		}
	}
	render(){
		let quote = this.props.quote
		let bg = ''
		if( this.props.background ){
			bg = new Image( this.props.background )
			bg.blur = true
			bg.bright = true
		}
		return(
			<div className="info" >
				<div className="bright-background" style={{ backgroundImage: `url(${bg})`}}>
					{ quote.context &&
						<section className="info__context context">
							<h2>Context</h2>
							<blockquote className="info__quote" dangerouslySetInnerHTML={quote.context}></blockquote>
							<cite className="info__attribution attribution">
								<div className="attribution__citation-details">{quote.attribution.citation_details}</div>
							</cite>
						</section>
					}
					{ quote.religion.map( ( r, i )=>{ return <div key={i} className="religion">{r}</div> }) }
					{ quote.commentary &&
						[
							<h2 key="a">Commentary</h2>,
							<div key="b" className="commentary" dangerouslySetInnerHTML={quote.commentary}></div>,
							<cite key="c">{quote.commentary_attribution}</cite>
						]
					}
					{ quote.resources &&
						[
							<h2 key="a">Resources</h2>,
							<ul key="b">{quote.resources.map( (r,i) => { return <li key={i}>{this.renderResource(r)}</li> } ) }</ul>
						]
					}
					{ quote.related_quotes &&
						[
							<h2 key="a">Related Quotes</h2>,
							<ul key="b">
								{
									quote.related_quotes.map(
										(r,i) => {
											let att = r.attribution
											let comma = att.person && att.publication ? ', ' : '';
											return (
												<li key={i}>
													<a target="_blank" href={r.permalink}>
														{r.title} - {att.person}{comma}{att.publication}
													</a>
												</li>
											)
										}
									)
								}
							</ul>
						]
					}
				</div>
			</div>
		)
	}
}
