import React from 'react'
import './root.sass'
import './style-components/fade-scroll-box.sass'

import { BrowserRouter, Route, Switch, withRouter, Redirect } from 'react-router-dom'
import { connect } from 'react-redux'
import { removeOrigin } from './router'
import { loadData } from '../lib/actions'


import * as API from '../lib/sacred-texts-rest'

import CSSTransitionGroup from './css-transition-group'
import FirstChild from './first-child'
import Page from './page'
import PictureQuote from './picture-quote.js'
import SiteHeader from './site-header.js'
import SiteNav from './site-nav.js'
import QuoteTerm from './quote-term'

import Image from '../lib/image'

class App extends React.Component {
	render(){
		if( ! this.props ){ return null }
		if( this.props.title ) document.title = 'Sacred Texts | ' + this.props.title
	
		return (
			<div className={ "react-root" + (this.props.loading ? ' loading' : '') } style={this.getStyles()} >
				<SiteNav
					{...this.props}
					{...this.state}
				/>
				<SiteHeader
					className="site-header"
					{...this.state}
				/>
				<CSSTransitionGroup component={FirstChild}>
					{ ! this.props.loading &&
						<div key='firstchild' className='loading-transition transition-group' >
							<Switch>
								<Route path='/daily-quote/:index' component={PictureQuote} />
								<Route path='/quote/:id' component={PictureQuote} />
								<Route path='/:tax/:term/:id' component={PictureQuote} />
								<Route path='/:tax/:term' component={QuoteTerm} />
								<Route path='/:pageSlug' component={Page} />
								<Route path='/' component={
									connect()(
										props => {
											API.get('/').then(
												data => {
													props.dispatch( loadData( { quotes: null } ) )
													props.history.replace( removeOrigin( data.redirect_to ) )
												}
											)
											return null
										}
									)
								} />
							</Switch>
						</div>
					}
				</CSSTransitionGroup>
			</div>
		)
	}
	componentDidMount(){
		window.addEventListener( 'resize', this.onWindowResize )
	}
	getStyles(){
		let bg = this.props.background ? this.props.background : `${API.root}/wp-content/uploads/2017/11/06-one-truth-ivana-cajina-e1510387445773.jpg`
		bg = new Image( bg )
		return { backgroundImage: 'url(' + bg + ')'}
	}
	onWindowResize = () => {
		this.setState(
			{
				windowWidth: window.innerWidth,
				windowHeight: window.innerHeight,
			}
		)
	}
}

const Root = withRouter(connect(
	( state = {} ) => {
		return Object.assign({ background: state.background, title: state.title } )
	}
)( App ))

export default function( props ){ return <BrowserRouter><Root /></BrowserRouter> }