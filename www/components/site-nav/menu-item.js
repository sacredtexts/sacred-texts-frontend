import './menu-item.sass'
import React from 'react'
import {NavLink} from '../router'

export default class extends React.Component {
 	static defaultProps = { className: '' }
	render(){
		return (
			<li className={`menu-item ${this.props.className}`} >
				<NavLink to={this.props.url}>{this.props.title}</NavLink>
			</li>
		)
	}
}
