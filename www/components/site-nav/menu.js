import './menu.sass'
import React from 'react'
import MenuItem from './menu-item'

export default class extends React.Component {
	static defaultProps = { itemClass: '' }
	render(){
		return (
			<ul className="menu">
				{ this.props.menu.items.map( ( i, k )=>{
					return <MenuItem className={this.props.itemClass} key={k} {...i} />
				} ) }
			</ul>
		)
	}
}
