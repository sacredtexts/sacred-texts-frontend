
import {Component} from 'react'
import Image from '../lib/image'
import React from 'react'
import {connect} from 'react-redux'
import { loadAPIPath } from '../lib/actions.js'

class Page extends Component{
	componentDidMount(){
		this.props.fetchPage( this.props.location.pathname )
	}
	componentDidUpdate( prevProps ){
		if( prevProps.location.pathname != this.props.location.pathname ){
			this.props.fetchPage( this.props.location.pathname)
		}
	}
	render(){
		let bg = new Image( this.props.background )
		bg.blur = true
		bg.bright = true
		return (
			<main className='main' >
				<div
					className='bright-background'
					style={{ backgroundImage: 'url('+bg+')'}}
					dangerouslySetInnerHTML={this.props.content}
				/>
			</main>
		)
	}
}

export default connect(
	( state={}, ownProps ) => {
		return {
			background: state.background,
			content: state.content
		}
	},
	dispatch => {
		return {
			fetchPage: path => { dispatch( loadAPIPath( path ) ) }
		}
	}
)( Page )
