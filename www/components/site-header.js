import React from 'react'
import {connect} from 'react-redux'
import { toggleNav } from '../lib/actions'
import './site-header.sass'

class SiteHeader extends React.Component {
	render(){
		return (
			<header className={`site-header ${ this.props.navOpen ? 'nav-open' : '' }`}>
				{ this.props.title && <span className="site-header__title" >{this.props.title}</span>}
				<button
					onClick={ this.props.toggleNav }
					className="dashicons dashicons-menu"
				/>
			</header>
		)
	}
}

export default connect(
	( state = {} )=> {
		return {title: state.title}
	},
	dispatch => {
		return {
			toggleNav: () => { dispatch( toggleNav() ) }
		}
	}
)(SiteHeader)