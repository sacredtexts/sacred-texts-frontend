import React from 'react'
import './site-nav.sass'
import TermsList from './terms-list'
import CSSTransitionGroup from 'react-transition-group/CSSTransitionGroup'
import FirstChild from './first-child'
import Menu from './site-nav/menu'
import {connect} from 'react-redux'
import { loadAPIPath } from '../lib/actions.js'


class SiteNav extends React.Component {
	componentDidMount(){
		this.props.fetchMenu( '/menu' )
	}
	render(){
		if( ! this.props.menus ){
			return null
		}
		let MenuComponent = {
			'Menu': Menu,
			'TermsList': TermsList
		}[this.props.menu.component]
		return (
			<CSSTransitionGroup
				component={FirstChild}
				transitionName="transition"
				transitionEnterTimeout={300}
				transitionLeaveTimeout={300}
				>
				{ this.props.navOpen &&
					<nav className="site-nav">
						<header>
							{ this.props.menu.parent &&
								<button
									className="dashicons dashicons-arrow-left-alt2"
								/>
							}
							<h2>{this.props.menu.title}</h2>
							<button
								onClick={ this.props.toggleNav }
								className="dashicons dashicons-no "
							/>
						</header>
						<div className="fade-scroll-box">
							<div className="fade-scroll-box__content">
								<MenuComponent {...this.props} itemClass="site-nav__item"/>
								<TermsList terms={this.props.menus.section.items} itemClass="site-nav__item"/>
							</div>
						</div>	
						<small className="copyright">{"\u00A9"} { new Date().getFullYear() } SacredTexts.Faith</small>
					</nav>
				}
			</CSSTransitionGroup>
		)
	}
}

export default connect(
	(state = {}, ownProps ) => {
		if( state.menu ){
			return Object.assign( {
				menu: state.menus[state.menu],
				menus: state.menus,
				navOpen: state.navOpen
			}, ownProps )
		}
		return {}
	},
	dispatch => {
		return {
			fetchMenu: path => { dispatch( loadAPIPath( path ) ) },
			toggleNav: () => { dispatch( { type: 'TOGGLE_NAV'} ) }
		}
	}
)(SiteNav)
