import React from 'react'
import './terms-list.sass'
import CSSTransitionGroup from 'react-transition-group/CSSTransitionGroup'
import FirstChild from './first-child'
import { NavLink } from './router'

export default class TermsList extends React.Component {
	static defaultProps = { itemClass: '' }
	render(){
		let terms = this.props.terms ? this.props.terms : this.props.menu.items
		return (
			<ol className="terms-list">
				{ terms.map( ( term )=>{
					let current = ( this.props.term && this.props.term.term_id == term.term_id ) ? 'current' : ''
					return (
						<li key={term.term_id} className={`count_${term.count} ${current} ${this.props.itemClass}`} >
							<NavLink to={ term.permalink } >{ term.name }</NavLink>
						</li>
					)
				} )}
			</ol>
		)
	}
}
