import React from 'react'
import { connect } from 'react-redux'
import { toggleInfo } from '../lib/actions'
import { Link } from './router'
import './arrow-nav.sass'

function ArrowNav( props ){
	return (
		<nav className="arrow-nav">
			<Link to={ props.prev ? props.prev: '' }>
				<button disabled={ ! props.prev } className="button dashicons dashicons-arrow-left-alt2" />
			</Link>
			<button className={ props.info ? 'dashicons dashicons-dismiss' : 'dashicons dashicons-info' } onClick={ props.toggleInfo } />
			<Link to={ props.next ? props.next : '' }>
				<button disabled={ ! props.next } className="button dashicons dashicons-arrow-right-alt2" />
			</Link>
		</nav>
	)
}

export default connect(
	undefined,
	dispatch => {
		return {
			toggleInfo: () => { dispatch( toggleInfo() ) }
		}
	}
)(ArrowNav)