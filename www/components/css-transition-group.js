import CSSTransitionGroup from 'react-addons-css-transition-group'
import React from 'react'

export default class extends CSSTransitionGroup {
	static defaultProps = {
		className: "transition-group",
		transitionName: "transition",
		transitionEnterTimeout: 500,
		transitionLeaveTimeout: 500,
		transitionAppearTimeout: 500
	}
	render(){ return <CSSTransitionGroup {...this.props} /> }
}
