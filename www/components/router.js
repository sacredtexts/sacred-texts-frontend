import React from 'react'
import { connect } from 'react-redux'
import { closeNav } from '../lib/actions'
import { NavLink as NL, Link as L, Redirect as R } from 'react-router-dom'
import FirstChild from './first-child'


export function removeOrigin( url ){
	return url.replace( window.location.origin, '' )
}

class Extender extends React.Component {
	get to(){
		let to = this.props.to
		to = removeOrigin( to )
		return to
	}
}

const props = dispatch => {
	return {
		onClick: () => { dispatch( closeNav() ) }
	}
}

export const NavLink = connect( undefined, props )(
	class extends Extender {
		render(){
			return <NL {...this.props} to={this.to} />
		}
	}
)

export const Link = connect( undefined, props )(
	class extends Extender {
		render(){
			if( this.to )
				return <L {...this.props} to={this.to} />
			else
				return <FirstChild {...this.props} />
		}
	}
)

export const Redirect = connect( undefined, props )(
	class extends Extender {
		render(){
			return <R {...this.props} to={this.to} />
		}
	}
) 