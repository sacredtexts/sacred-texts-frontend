const webpack = require( 'webpack' )

module.exports = {
  entry: './index.js',
	module: {
		rules: [
			{
				test: /\.js$/,
        exclude: /node_modules/,
				use: {
					loader: 'babel-loader',
					options: {
						presets: ["es2015", "react"],
						plugins: ["transform-class-properties"]
					}
				}
			},
			{
				test: /\.sass$/,
        exclude: /node_modules/,
				use: [
          { loader: 'style-loader', options: { sourceMap: true } },
          { loader: 'css-loader', options: { sourceMap: true, url: false } },
          { loader: 'resolve-url-loader', options: { sourceMap: true } },
          { loader: 'sass-loader', options: { sourceMap: true } }
        ]
			}
		]
	},
	plugins: [],
	devtool: 'source-map'
};
