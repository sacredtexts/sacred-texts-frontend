import * as API from '../lib/sacred-texts-rest'
import { removeOrigin } from '../components/router'
/*
 * Action Types
 */

export const CLOSE_NAV = 'CLOSE_NAV'
export const LOAD_DATA = 'LOAD_DATA'
export const LOAD_QUOTE = 'LOAD_QUOTE'
export const TOGGLE_INFO = 'TOGGLE_INFO'
export const TOGGLE_NAV = 'TOGGLE_NAV'

/*
 * Action Creators
 */
export function closeNav(){
	return { type: CLOSE_NAV }
}
export function loadAPIPath( path  = '/' ){
	return dispatch => {
		return API.get( path )
		.then(
			pageData => {
				dispatch( loadData( pageData ) )
			}
		)
	}
}

export function loadData( data ){
	return {
		type: LOAD_DATA,
		data
	}
}
export function loadQuote( quote ){
	return {
		type: LOAD_QUOTE,
		quote
	}
}
export function fetchQuote( params ){
	return dispatch => {
		dispatch( loadQuote() )
		if( params.id ){
			return API.get( '/quote/' + params.id )
			.then(
				data => {
					dispatch( loadData( data ) )
				}
			)
		} else {
			console.log( 'fetchQuote missing params')
		}
	}
}
export function fetchTerm( path ){
	return dispatch => {
		return dispatch( loadAPIPath( `/${path.tax}/${path.term}` ) )
	}
}

export function toggleInfo(){
	return {
		type: TOGGLE_INFO
	}
}
export function toggleNav(){
	return {
		type: TOGGLE_NAV
	}
}