switch ( window.location.hostname ) {
	case 'localhost':
		var root = 'http://localhost'
		break;
	case 'dev':
		var root = 'http://dev'
		break;
	default:
		var root = 'https://wordpress.sacredtexts.faith'
}

function get( path ){
	path =  path ? path : ''
	path = path.endsWith( '/' ) ? path : path + '/'
	let request = new XMLHttpRequest()
	let promise = new Promise( ( resolve, reject )=> {
		request.onload = () => {
			resolve( JSON.parse( request.responseText ) )
		}
		request.onerror = e => {
			reject( e )
		}
	})
	request.open('GET', `${root}${path}?json`, true)
	request.send()
	return promise
}

export {get, root}