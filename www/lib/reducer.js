import { TOGGLE_INFO, LOAD_QUOTE, CLOSE_NAV } from './actions'
export default ( oldState, action ) =>{
	switch (action.type) {
		case CLOSE_NAV:
			return Object.assign( {}, oldState, { navOpen: false } )
		case 'LOAD_DATA':
			return Object.assign( {}, oldState, action.data )
		case LOAD_QUOTE:
			return Object.assign( {}, oldState, { quote: action.quote } )
		case TOGGLE_INFO:
			return Object.assign( {}, oldState, { infoOpen : ! oldState.infoOpen })
		case 'TOGGLE_NAV':
			return Object.assign( {}, oldState, { navOpen : ! oldState.navOpen } )
		default:
			return oldState
	}
}