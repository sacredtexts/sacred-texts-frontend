import ImgixClient from 'imgix-core-js'
import round from 'round'

export default class {
	constructor( source ){
		if( source ){
			source = new URL( source )
			this.path = source.pathname
		}
	}
	toString(){
		if( this.path ){
			let client = new ImgixClient( {host:'sacredtexts.imgix.net' } )
			let opts = {
				w: round( window.innerWidth, 100, 'up' ),
				h: round( window.innerHeight, 100, 'up' ),
				auto: 'format'
			}
			if( this.blur ){ opts.blur = 100 }
			if( this.bright ){
				opts.bm = 'normal'
				opts.blend = '9FFF'
			}
			return client.buildURL( this.path, opts )
		} else {
			return ''
		}
	}
}
