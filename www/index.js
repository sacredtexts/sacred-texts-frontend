import React from 'react'
import {render} from 'react-dom'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import thunkMiddleware from 'redux-thunk'
import { createLogger } from 'redux-logger'
import Root from './components/root.js'
import * as API from './lib/sacred-texts-rest'
import reducer from './lib/reducer'

document.addEventListener('touchstart', function(){
	document.documentElement.classList.add('touch')
})

const isDev = process.env.NODE_ENV == 'development'

let middleware = [ thunkMiddleware ]
if( isDev ){ middleware.push( createLogger() ) }
const store = createStore(
	reducer,
	applyMiddleware( ...middleware )
)
render(
	<Provider store={store}><Root /></Provider>,
	document.getElementsByClassName('react-container')[0]
)
